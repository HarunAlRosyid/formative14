-- using database production
USE production;

-- create table brand
CREATE TABLE brand (
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20) NOT NULL);

-- delete column brand
ALTER TABLE product
	DROP COLUMN brand;
    
-- add column brand_id
ALTER TABLE product
	ADD COLUMN brand_id INT NOT NULL;

SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE product 
	ADD FOREIGN KEY (brand_id) REFERENCES Brand(id);

SELECT id, name, description, art_number, price, stock, is_delete, brand_id 
	FROM product;




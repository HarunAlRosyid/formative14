-- using database staging
USE staging;

-- insert table product_clientA
INSERT INTO  product_clientA (name, description, artnumber, price, stock, deleted, brand_name)  
VALUES 
	('Mi Mix 4', 'Smartphone under display camera', 123456789, 10000000, 10,0,'Xiaomi'),
	('Mi 11', 'Real flagship', 123456790, 13000000, 20,0,'Xiaomi'),
    ('Mi 11 Ultra', 'Ultra flagship', 123456791, 17000000, 30,0,'Xiaomi'),
    ('Mi 11T', 'flagship budget', 123456792, 8000000, 40,0,'Xiaomi'),
    ('Mi 11 Lite', 'Midrange', 123456794, 4000000, 50,0,'Xiaomi'),
    ('Xperia One I', 'GEN 1 One series', 123456795, 18000000, 10,0,'SONY'),
    ('Xperia One II', 'GEN 2 One series', 123456796, 19000000, 20,0,'SONY'),
    ('Xperia One III', 'GEN 3 One series', 123456797, 20000000, 30,0,'SONY'),
    ('Xperia 10 I', 'Midrange Gen 1', 123456798, 10000000, 40,0,'SONY'),
    ('Xperia 10 II', 'Midrange Gen 2', 123456799, 12000000, 50,0,'SONY');
    
-- show table product_clientA
SELECT id, name, description, artnumber, price, stock, deleted, brand_name 
	FROM product_clientA;
    
-- insert table product_clientB
INSERT INTO  product_clientB (nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek)  
VALUES 
	('Z Fold 3', 'Foldable tablet', 223456789, 15000000, 10,0,'Samsung'),
	('Z Flip 3', 'foldable smartphone', 223456790, 12000000, 20,0,'Samsung'),
    ('S 21 Ultra', 'Ultra flagship', 223456791, 17000000, 30,0,'Samsung'),
    ('S 21', 'flagship', 223456792, 11000000, 40,0,'Samsung'),
    ('S 21 FE', 'fun edition', 223456794, 9000000, 50,0,'Samsung'),
    ('Iphone 10', 'iphone gen 10', 223456795, 18000000, 10,0,'Apple'),
    ('Iphone 11', 'iphone gen 11', 223456796, 19000000, 20,0,'Apple'),
    ('Iphone 12', 'iphone gen 12', 223456797, 20000000, 30,0,'Apple'),
    ('Iphone 13', 'iphone gen 13', 223456798, 21000000, 40,0,'Apple'),
    ('Iphone 13 Pro', 'iphone gen 13', 223456799, 25000000, 50,0,'Apple');
    
-- show table product_clientB    
SELECT id, nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek 
	FROM product_clientB; 

    
    
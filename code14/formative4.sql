-- using database production
USE production;

-- insert table product
INSERT INTO  product (name, description, art_number, price, stock, is_delete, brand)  
	SELECT name, description, artnumber, price, stock, deleted, brand_name 
    FROM staging.product_clientA;
INSERT INTO  product (name, description, art_number, price, stock, is_delete, brand)  
	SELECT  nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek 
    FROM staging.product_clientB;

-- show table product   
SELECT id, name, description, art_number, price, stock, is_delete, brand 
	FROM product;

    
    
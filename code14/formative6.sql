-- using database production
USE production;

-- insert table product
INSERT INTO  brand (name)
VALUES 
	('Xiaomi'),
    ('SONY'),
    ('Samsung'),
    ('Apple');
    
    
-- show table brand   
SELECT id, name
	FROM brand;
    
-- delete all record product
DELETE FROM product;
SELECT *
	FROM product;
    
-- insert table product with checking table brand
INSERT INTO  product (name, description, art_number, price, stock, is_delete, brand_id)
	SELECT staging.product_clientA.name, staging.product_clientA.description, staging.product_clientA.artnumber, 
	   staging.product_clientA.price, staging.product_clientA.stock, staging.product_clientA.deleted,  
       production.brand.id  FROM staging.product_clientA 
	LEFT JOIN production.brand 
       ON product_clienta.brand_name = production.brand.name;
       
INSERT INTO  product (name, description, art_number, price, stock, is_delete, brand_id)
    SELECT staging.product_clientB.nama, staging.product_clientB.deskripsi, staging.product_clientB.nomor_artikel, 
	   staging.product_clientB.harga, staging.product_clientB.persediaan_barang, staging.product_clientB.dihapus,  
       production.brand.id  FROM staging.product_clientB 
	LEFT JOIN production.brand 
       ON product_clientB.nama_merek = production.brand.name;
   
-- show table product   
SELECT id, name, description, art_number, price, stock, is_delete, brand_id 
	FROM product;
    
    

    


    
    
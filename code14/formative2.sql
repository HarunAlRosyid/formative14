-- using database staging
USE staging;

-- create table product clieant A
CREATE TABLE product_clientA (
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(256) NULL,
    artnumber INT UNIQUE NOT NULL,
    price DECIMAL(20,2) NOT NULL,
    stock INT NOT NULL DEFAULT 0,
    deleted INT NOT NULL DEFAULT 0,
    brand_name VARCHAR(20) NOT NULL
);

-- create table product clieant B
CREATE TABLE product_clientB (
	id INT AUTO_INCREMENT PRIMARY KEY,
    nama VARCHAR(20) NOT NULL,
    deskripsi VARCHAR(256) NULL,
    nomor_artikel INT UNIQUE NOT NULL,
    harga DECIMAL(20,2) NOT NULL,
    persediaan_barang INT NOT NULL DEFAULT 0,
    dihapus INT NOT NULL DEFAULT 0,
    nama_merek VARCHAR(20) NOT NULL
);

-- using database staging
USE production;

-- create table product clieant A
CREATE TABLE product (
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(256) NULL,
    art_number INT UNIQUE NOT NULL,
    price DECIMAL(20,2) NOT NULL,
    stock INT NOT NULL DEFAULT 0,
    is_delete INT NOT NULL DEFAULT 0,
    brand VARCHAR(20) NOT NULL
);

